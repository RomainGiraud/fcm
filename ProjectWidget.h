/**
 * @file  ProjectWidget.h
 *
 * The table in the center of the programme to display the model (cards)
 *
 */

#ifndef PROJECT_H
#define PROJECT_H

#include <QTableView>

class QSqlDatabase;
class QSqlTableModel;
class QSqlRecord;

class ProjectWidget : public QTableView
{
    Q_OBJECT

    public:
        ProjectWidget(QWidget *parent=0);
        ~ProjectWidget();
        void load(const QString &fileName);
        void save();
        void retranslateUi();

    signals:
        void modified();

    public slots:
    	void addCard();
    	void removeCard();

    private:
    	QString 		_fileName;
        QSqlTableModel *_model;
        void createTables(const QSqlDatabase &db);

    private slots:
        void beforeAddCard(QSqlRecord &record);
};

#endif /*PROJECT_H*/
