/**
 * @file  DBConnection.h
 *
 * Return a singleton of the database connection
 *
 */

#ifndef DBCONNECTION_H
#define DBCONNECTION_H

class QString;
class QSqlDatabase;

class DBConnection
{
    public:
        static QSqlDatabase openDB (const QString &fileName);
        static void         closeDB(const QString &fileName);

    private:
        DBConnection() {}
};

#endif /*DBCONNECTION_H*/
