/**
 * @file  ConfigDialog.h
 *
 * Dialog Box to configure the programme
 *
 */

#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <QLocale>

class QListWidget;
class QListWidgetItem;
class QStackedWidget;

class ConfigDialog : public QDialog
{
    Q_OBJECT

    public:
        ConfigDialog(const QLocale &locale);
        QLocale getLocale();

    public slots:
        void changePage(QListWidgetItem *current, QListWidgetItem *previous);

    private:
        void createIcons();

        QListWidget *contentsWidget;
        QStackedWidget *pagesWidget;
};

#endif /*CONFIGDIALOG_H*/
