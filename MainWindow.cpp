#include <QtGui>
#include "MainWindow.h"
#include "BaseException.h"
#include "SqlException.h"
#include "LearnDialog.h"
#include "GraphDialog.h"
#include "ConfigDialog.h"

MainWindow::MainWindow()
{
    readSettings();

    qApp->installTranslator(&_translator);

    _project = new ProjectWidget;
    _project->load(_dbName);
    connect(_project,SIGNAL(modified()),this,SLOT(projectModified()));

    setCentralWidget(_project);

    createActions();
    createMenus();
    createToolBars();
    createContextMenu();
    retranslateUi();

    saveAction->setEnabled(false);

    setWindowTitle("Flash Card Manager [*]");
    setWindowIcon(QIcon(":/icons/icon.png"));
}

void MainWindow::learnDialog()
{
    _project->setVisible(false);
    LearnDialog diag (_dbName);
    _project->setVisible(true);
}

void MainWindow::graphDialog()
{
    GraphDialog diag (_dbName,0);
}

void MainWindow::createContextMenu()
{
    _project->addAction(addCardAction);
    _project->addAction(removeCardAction);
    _project->setContextMenuPolicy(Qt::ActionsContextMenu);
}

void MainWindow::projectModified()
{
    saveAction->setEnabled(true);
    setWindowModified(true);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (okToContinue()) {
        writeSettings();
        event->accept();
    } else {
        event->ignore();
    }
}

void MainWindow::about()
{
    QMessageBox::about(this, tr("About"),
              "<h2>Flash Cards Manager 0.1</h2>"
              "<p>Copyright &copy; 2008 Tipoun");
}

void MainWindow::createActions()
{
    saveAction = new QAction(this);
    saveAction->setIcon(QIcon(":/images/save.png"));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));

    configAction = new QAction(this);
    connect(configAction, SIGNAL(triggered()), this, SLOT(config()));

    exitAction = new QAction(this);
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));

    addCardAction = new QAction(this);
    connect(addCardAction, SIGNAL(triggered()), _project, SLOT(addCard()));

    removeCardAction = new QAction(this);
    connect(removeCardAction, SIGNAL(triggered()), _project, SLOT(removeCard()));

    learnAction = new QAction(this);
    connect(learnAction, SIGNAL(triggered()), this, SLOT(learnDialog()));

    graphAction = new QAction(this);
    connect(graphAction, SIGNAL(triggered()), this, SLOT(graphDialog()));

    aboutAction = new QAction(this);
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAction = new QAction(this);
    connect(aboutQtAction, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
}

void MainWindow::createMenus()
{
    fileMenu = new QMenu(this);
    fileMenu->addAction(saveAction);
    fileMenu->addAction(configAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    editMenu = new QMenu(this);
    editMenu->addAction(addCardAction);
    editMenu->addAction(removeCardAction);

    toolsMenu = new QMenu(this);
    toolsMenu->addAction(learnAction);
    toolsMenu->addAction(graphAction);

    menuBar()->addSeparator();

    helpMenu = new QMenu(this);
    helpMenu->addAction(aboutAction);
    helpMenu->addAction(aboutQtAction);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(editMenu);
    menuBar()->addMenu(toolsMenu);
    menuBar()->addMenu(helpMenu);
}

void MainWindow::createToolBars()
{
    fileToolBar = new QToolBar(0);
    fileToolBar->addAction(saveAction);
}

void MainWindow::retranslateUi()
{
    _translator.load("translations/fcm_" + _locale.name());

    fileMenu->setTitle(tr("&File"));
    editMenu->setTitle(tr("&Edit"));
    toolsMenu->setTitle(tr("&Tools"));
    helpMenu->setTitle(tr("&Help"));

    saveAction->setText(tr("&Save"));
    saveAction->setShortcut(tr("Ctrl+S"));
    saveAction->setStatusTip(tr("Save the changes"));
    configAction->setText(tr("&Preferences"));
    configAction->setShortcut(tr("Ctrl+P"));
    configAction->setStatusTip(tr("Modify preferences"));
    exitAction->setText(tr("E&xit"));
    exitAction->setShortcut(tr("Ctrl+Q"));
    exitAction->setStatusTip(tr("Exit the application"));
    addCardAction->setText(tr("&Add a card"));
    addCardAction->setStatusTip(tr("Add a card"));
    removeCardAction->setText(tr("&Remove a card"));
    removeCardAction->setStatusTip(tr("Remove a card"));
    learnAction->setText(tr("&Learn"));
    learnAction->setStatusTip(tr("Learn cards"));
    graphAction->setText(tr("&Graph"));
    graphAction->setStatusTip(tr("Display an evolution graph"));
    aboutAction->setText(tr("&About"));
    aboutAction->setStatusTip(tr("Show the application's About box"));
    aboutQtAction->setText(tr("About &Qt"));
    aboutQtAction->setStatusTip(tr("Show the Qt library's About box"));

    _project->retranslateUi();
}

void MainWindow::readSettings()
{
    QSettings settings("fcm");

    _dbName = settings.value("database_filename",DatabaseFilename).toString();
    _locale = settings.value("locale",QLocale(QLocale::English)).toLocale();
    QRect rect = settings.value("geometry", QRect(200, 200, 500, 400)).toRect();
    move(rect.topLeft());
    resize(rect.size());
}

void MainWindow::writeSettings()
{
    QSettings settings("fcm");
    settings.setValue("database_filename", _dbName);
    settings.setValue("locale",_locale);
    settings.setValue("geometry", geometry());
}

bool MainWindow::okToContinue()
{
    if (isWindowModified()) {
        int r = QMessageBox::warning(this, "Flash Cards Manager",
                        tr("The document has been modified.\n"
                           "Do you want to save your changes?"),
                        QMessageBox::Yes | QMessageBox::Default,
                        QMessageBox::No,
                        QMessageBox::Cancel | QMessageBox::Escape);
        if (r == QMessageBox::Yes) {
            return save();
        } else if (r == QMessageBox::Cancel) {
            return false;
        }
    }
    return true;
}

void MainWindow::config()
{
    ConfigDialog dial(_locale);
    dial.exec();
    QLocale temp = dial.getLocale();
    if (_locale != temp)
    {
        _locale = temp;
        retranslateUi();
    }
}

bool MainWindow::save()
{
	try
	{
		_project->save();
	}
	catch(BaseException &e)
	{
	    QMessageBox::warning(this, e.getTitle(), e.getMessage());
        statusBar()->showMessage(tr("Saving canceled"), 2000);
        return false;
	}

    saveAction->setEnabled(false);
    setWindowModified(false);
    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}
