/**
 * @file  MainWindow.h
 *
 * The main window
 *
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTranslator>
#include <QLocale>
#include <QDir>
#include "ProjectWidget.h"

class QMenu;
class QToolBar;
class QAction;

const QString DatabaseFilename = QDir::homePath()+"/.fcm.db";

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        MainWindow();

    protected:
        void closeEvent(QCloseEvent *event);

    private slots:
        bool save();
        void config();
        void about();
        void learnDialog();
        void graphDialog();
        void projectModified();

    private:
        void createActions();
        void createMenus();
        void createToolBars();
        void createContextMenu();
        void retranslateUi();

        void readSettings();
        void writeSettings();

        bool okToContinue();

        QString _dbName;
        QLocale _locale;
        ProjectWidget *_project;
        QTranslator _translator;

        QMenu *fileMenu;
        QMenu *editMenu;
        QMenu *toolsMenu;
        QMenu *optionsMenu;
        QMenu *helpMenu;
        QToolBar *fileToolBar;
        QToolBar *editToolBar;
        QAction *saveAction;
        QAction *configAction;
        QAction *exitAction;
        QAction *addCardAction;
        QAction *removeCardAction;
        QAction *learnAction;
        QAction *graphAction;
        QAction *aboutAction;
        QAction *aboutQtAction;
};

#endif /*MAINWINDOW_H*/
