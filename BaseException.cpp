#include "BaseException.h"

BaseException::BaseException(const QString &message)
	: _message(message)
{
}

const QString& BaseException::getMessage()
{
	return _message;
}

QString BaseException::getTitle()
{
	return "Base error";
}
