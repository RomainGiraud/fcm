/**
 * @file  GraphDialog.h
 *
 * Display the evolution's graph
 *
 */

#ifndef GRAPHDIALOG_H
#define GRAPHDIALOG_H

#include <QDialog>

class GraphDialog : public QDialog
{
    public:
        GraphDialog(QString dbName, int cardID = -1, QWidget *parent = 0);
};

#endif /*GRAPHDIALOG_H*/
