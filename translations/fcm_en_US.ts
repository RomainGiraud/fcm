<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../ConfigDialog.cpp" line="20"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ConfigDialog.cpp" line="42"/>
        <source>Config Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ConfigDialog.cpp" line="55"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfigurationPage</name>
    <message>
        <location filename="../ConfigurationPage.cpp" line="8"/>
        <source>General configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ConfigurationPage.cpp" line="10"/>
        <source>Language:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LearnDialog</name>
    <message>
        <location filename="../LearnDialog.cpp" line="36"/>
        <source>Skip card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="38"/>
        <source>Show answer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="40"/>
        <source>I know it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="43"/>
        <source>I don&apos;t know it</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ProjectWidget.cpp" line="22"/>
        <source>Data 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="23"/>
        <source>Comment 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="24"/>
        <source>Data 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="25"/>
        <source>Comment 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="144"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="145"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="146"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="147"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="149"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="150"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="151"/>
        <source>Save the changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="152"/>
        <source>&amp;Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="153"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="154"/>
        <source>Modify preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="155"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="156"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="157"/>
        <source>Exit the application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="158"/>
        <source>&amp;Add a card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="159"/>
        <source>Add a card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="160"/>
        <source>&amp;Remove a card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="161"/>
        <source>Remove a card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="162"/>
        <source>&amp;Learn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="163"/>
        <source>Learn cards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="164"/>
        <source>&amp;Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="165"/>
        <source>Display an evolution graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="166"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="167"/>
        <source>Show the application&apos;s About box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="168"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="198"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="232"/>
        <source>Saving canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="238"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
