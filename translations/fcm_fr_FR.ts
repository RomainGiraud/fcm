<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="fr">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="../ConfigDialog.cpp" line="20"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../ConfigDialog.cpp" line="42"/>
        <source>Config Dialog</source>
        <translation>Panneau de configuration</translation>
    </message>
    <message>
        <location filename="../ConfigDialog.cpp" line="55"/>
        <source>Configuration</source>
        <translation>Configuration</translation>
    </message>
</context>
<context>
    <name>ConfigurationPage</name>
    <message>
        <location filename="../ConfigurationPage.cpp" line="10"/>
        <source>Language:</source>
        <translation>Langue:</translation>
    </message>
    <message>
        <location filename="../ConfigurationPage.cpp" line="8"/>
        <source>General configuration</source>
        <translation>Configuration générale</translation>
    </message>
</context>
<context>
    <name>LearnDialog</name>
    <message>
        <location filename="../LearnDialog.cpp" line="36"/>
        <source>Skip card</source>
        <translation>Passer la carte</translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="38"/>
        <source>Show answer</source>
        <translation>Montrer la réponse</translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="40"/>
        <source>I know it</source>
        <translation>Je connais</translation>
    </message>
    <message>
        <location filename="../LearnDialog.cpp" line="43"/>
        <source>I don&apos;t know it</source>
        <translation>Je ne connais pas</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ProjectWidget.cpp" line="22"/>
        <source>Data 1</source>
        <translation>Donnée 1</translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="23"/>
        <source>Comment 1</source>
        <translation>Commentaire 1</translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="24"/>
        <source>Data 2</source>
        <translation>Donnée 2</translation>
    </message>
    <message>
        <location filename="../ProjectWidget.cpp" line="25"/>
        <source>Comment 2</source>
        <translation>Commentaire 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="144"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="145"/>
        <source>&amp;Edit</source>
        <translation>É&amp;dition</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="146"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="147"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="149"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="150"/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="151"/>
        <source>Save the changes</source>
        <translation>Sauvegarder les modifications</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="152"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Préférences</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="153"/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="154"/>
        <source>Modify preferences</source>
        <translation>Modifier les préférences</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="155"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="156"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="157"/>
        <source>Exit the application</source>
        <translation>Quitter le programme</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="158"/>
        <source>&amp;Add a card</source>
        <translation>&amp;Ajouter une carte</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="159"/>
        <source>Add a card</source>
        <translation>Ajout d&apos;une carte</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="160"/>
        <source>&amp;Remove a card</source>
        <translation>&amp;Supprimer une carte</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="161"/>
        <source>Remove a card</source>
        <translation>Supprime la carte</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="162"/>
        <source>&amp;Learn</source>
        <translation>&amp;Apprendre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="163"/>
        <source>Learn cards</source>
        <translation>Lancer l&apos;apprentissage</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="164"/>
        <source>&amp;Graph</source>
        <translation>&amp;Graphique</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="165"/>
        <source>Display an evolution graph</source>
        <translation>Afficher le graphique de progression</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="166"/>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="167"/>
        <source>Show the application&apos;s About box</source>
        <translation>Affiche la boite de dialogue À propos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="168"/>
        <source>About &amp;Qt</source>
        <translation>À propos de &amp;Qt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="169"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Affiche la boite de dialogue À propos de Qt</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="198"/>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Le document a été modifié.
Voulez-vous sauvegarder les modifications ?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="232"/>
        <source>Saving canceled</source>
        <translation>Sauvegarde annulée</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="238"/>
        <source>File saved</source>
        <translation>Fichier sauvé</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="70"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
</context>
</TS>
