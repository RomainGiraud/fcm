#include <QtGui>

#include "ConfigurationPage.h"

ConfigurationPage::ConfigurationPage(const QLocale &locale, QWidget *parent)
    : QWidget(parent)
{
    QGroupBox *configGroup = new QGroupBox(tr("General configuration"));

    QLabel *langLabel = new QLabel(tr("Language:"));

    _langCombo = new QComboBox;
    QStringList fileNames = QDir("translations").entryList(QStringList("fcm_*.qm"));
    for (int i (0); i < fileNames.size(); ++i)
    {
        QString l = fileNames[i];
        l.remove(0, l.indexOf('_') + 1);
        l.chop(3);

        QLocale loc (l);

        _langCombo->addItem(QLocale::languageToString(loc.language()),loc);
        if (loc == locale)
            _langCombo->setCurrentIndex(i);
    }

    QHBoxLayout *serverLayout = new QHBoxLayout;
    serverLayout->addWidget(langLabel);
    serverLayout->addWidget(_langCombo);

    QVBoxLayout *configLayout = new QVBoxLayout;
    configLayout->addLayout(serverLayout);
    configGroup->setLayout(configLayout);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(configGroup);
    mainLayout->addStretch(1);
    setLayout(mainLayout);
}

QLocale ConfigurationPage::getLocale()
{
    return _langCombo->itemData(_langCombo->currentIndex()).toLocale();
}
