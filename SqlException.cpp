#include <QtSql>
#include "SqlException.h"

SqlException::SqlException(const QSqlError &error)
	: _error(error)
{
	_message = _error.text();
}
SqlException::SqlException(const QString &dbName)
{
	SqlException(QSqlDatabase::database(dbName).lastError());
}

QString SqlException::getTitle()
{
	return "Database error";
}
