/**
 * @file  SqlException.h
 *
 * Exception for sql error
 *
 */

#ifndef SQLEXCEPTION_H
#define SQLEXCEPTION_H

#include "BaseException.h"
#include <QSqlError>

class SqlException : public BaseException
{
	public:
		SqlException(const QSqlError &error);
		SqlException(const QString &dbName);
		virtual ~SqlException() throw() {}
		virtual QString getTitle();
		
	protected:
		QSqlError _error;
};

#endif /*SQLEXCEPTION_H*/
