#include <QtGui>
#include <QtSql>
#include "DBConnection.h"
#include "BaseException.h"
#include "SqlException.h"

QSqlDatabase DBConnection::openDB(const QString &fileName)
{
	if (fileName.isEmpty())
		throw BaseException("The database name cannot be empty.");
	
	if (QSqlDatabase::contains(fileName))
		return QSqlDatabase::database(fileName);
	
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE",fileName);
    db.setDatabaseName(fileName);
    if (!db.open())
    	throw SqlException (db.lastError());

    return db;
}

void DBConnection::closeDB(const QString &fileName)
{
	if (fileName.isEmpty()) return;
	
	QSqlDatabase::removeDatabase(fileName);
}
