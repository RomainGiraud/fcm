#include <QtGui>
#include <QtSql>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include "GraphDialog.h"
#include "DBConnection.h"

GraphDialog::GraphDialog(QString dbName, int cardID, QWidget *parent)
    : QDialog(parent)
{
    QSqlDatabase db = DBConnection::openDB(dbName);

    if (cardID == -1)
    {
        QSqlQuery q1 ("SELECT COUNT(*) FROM sessions",db);
        QSqlQuery q2 ("SELECT date_start, date_end FROM sessions",db);
        if (!q1.next()) return;

        QTime t;
        while (q2.next())
        {
            QDateTime t1 = QDateTime::fromTime_t (q2.record().value(0).toUInt());
            QDateTime t2 = QDateTime::fromTime_t (q2.record().value(1).toUInt());
            t = QTime(0,0).addSecs(t1.secsTo(t2));
        }

        QLabel *l1 = new QLabel (QString("Number of sessions: ")+q1.record().value(0).toString());
        QLabel *l2 = new QLabel (QString("Time average: ")+t.toString());

        QSqlQuery query1 ("SELECT COUNT(*) FROM marks",db);
        QSqlQuery query2 ("SELECT COUNT(*) FROM marks WHERE is_pass=1",db);

        if (!query1.next() || !query2.next()) return;

        QLabel *label = new QLabel (QString("Result: ")+query2.record().value(0).toString()+"/"+query1.record().value(0).toString());
        QHBoxLayout *layout = new QHBoxLayout;
        layout->addWidget(label);
        setLayout(layout);
    }
    else if (cardID == 0)
    {
        QSqlQuery q ("SELECT sessions.id, COUNT(marks.is_pass) as total, "
                        "COUNT(case when is_pass=1 then is_pass end) as pass, "
                        "COUNT(case when is_pass=0 then is_pass end) as fail "
                    "FROM sessions, marks "
                    "WHERE marks.date BETWEEN sessions.date_start AND sessions.date_end "
                    "GROUP BY sessions.id",db);

        QVector <double> x(0), total(0), pass(0), fail(0);
        while (q.next())
        {
            x.push_back(q.record().value(0).toInt());
            total.push_back(q.record().value(1).toInt());
            pass.push_back(q.record().value(2).toInt());
            fail.push_back(q.record().value(3).toInt());
        }

        QwtPlot *plot = new QwtPlot(QwtText("Evolution" ), this);
        QwtPlotCurve *curve1 = new QwtPlotCurve("Total" );
        QwtPlotCurve *curve2 = new QwtPlotCurve("Pass" );
        QwtPlotCurve *curve3 = new QwtPlotCurve("Fail" );

        curve1->setData(x.data(), total.data(), x.size());
        curve1->setPen(QPen(Qt::red));
        curve1->attach(plot);
        curve2->setData(x.data(), pass.data(), x.size());
        curve2->setPen(QPen(Qt::blue));
        curve2->attach(plot);
        curve3->setData(x.data(), fail.data(), x.size());
        curve3->setPen(QPen(Qt::yellow));
        curve3->attach(plot);
        plot->replot();
        setFixedSize(plot->sizeHint());
    }
    else
    {
        QSqlQuery query (db);
        query.prepare("SELECT * FROM marks WHERE card_id=:id");
        query.bindValue(":id",cardID);
        query.exec();

        QVector <double> x(0), y(0);
        while (query.next())
        {
            x.push_back(query.value(2).toInt());
            y.push_back(query.value(3).toInt());
        }

        QwtPlot *plot = new QwtPlot(QwtText("Evolution" ), this);
        QwtPlotCurve *curve = new QwtPlotCurve("Curve" );

        curve->setData(x.data(), y.data(), 100);
        curve->attach(plot);
        plot->replot();
        setFixedSize(plot->sizeHint());
    }

    exec();
}
