/**
 * @file  BaseException.h
 *
 * Exception with a string in parameter
 *
 */

#ifndef BASEEXCEPTION_H
#define BASEEXCEPTION_H

#include <exception>
#include <QString>

class BaseException : public std::exception
{
	public:
		BaseException(const QString &message=QString());
		virtual ~BaseException() throw() {}
		virtual const QString& getMessage();
		virtual QString getTitle();
		
	protected:
		QString _message;
};

#endif /*BASEEXCEPTION_H*/
