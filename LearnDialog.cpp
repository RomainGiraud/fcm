#include <QtGui>
#include <QtSql>
#include "LearnDialog.h"
#include "DBConnection.h"
#include "SqlException.h"
#include "MainWindow.h"

LearnDialog::LearnDialog(QString dbName, QWidget *parent)
    : QDialog(parent), _index(0), _db (DBConnection::openDB(dbName))
{
    QLabel *labelData1    = new QLabel (MainWindow::tr("Data 1"));
    QLabel *labelComment1 = new QLabel (MainWindow::tr("Comment 1"));
    QLabel *labelData2    = new QLabel (MainWindow::tr("Data 2"));
    QLabel *labelComment2 = new QLabel (MainWindow::tr("Comment 2"));

    _viewData1 = new QTextEdit;
    _viewData1->setReadOnly(true);
    _viewComment1 = new QTextEdit;
    _viewComment1->setReadOnly(true);
    _viewData2 = new QTextEdit;
    _viewData2->setReadOnly(true);
    _viewComment2 = new QTextEdit;
    _viewComment2->setReadOnly(true);

    _model = new QSqlQueryModel;
    _model->setQuery("SELECT * FROM cards",_db);

    _mapper = new QDataWidgetMapper;
    _mapper->setModel(_model);
    _mapper->addMapping(_viewData1,1);
    _mapper->addMapping(_viewComment1,2);
    _mapper->addMapping(_viewData2,3);
    _mapper->addMapping(_viewComment2,4);
    _mapper->toFirst();

    _skipButton = new QPushButton (tr("Skip card"));
    connect(_skipButton,SIGNAL(clicked(bool)),this,SLOT(skipCard()));
    _showButton = new QPushButton (tr("Show answer"));
    connect(_showButton,SIGNAL(clicked(bool)),this,SLOT(showAnswer()));
    _yesButton = new QPushButton (tr("I know it"));
    _yesButton->setVisible(false);
    connect(_yesButton,SIGNAL(clicked(bool)),this,SLOT(okCard()));
    _noButton = new QPushButton (tr("I don't know it"));
    _noButton->setVisible(false);
    connect(_noButton,SIGNAL(clicked(bool)),this,SLOT(notOkCard()));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(labelData1,0,0);
    layout->addWidget(_viewData1,0,1);
    layout->addWidget(labelComment1,1,0);
    layout->addWidget(_viewComment1,1,1);
    layout->addWidget(labelData2,2,0);
    layout->addWidget(_viewData2,2,1);
    layout->addWidget(labelComment2,3,0);
    layout->addWidget(_viewComment2,3,1);
    layout->addWidget(_skipButton,4,0);
    layout->addWidget(_showButton,4,1);
    layout->addWidget(_yesButton,5,0);
    layout->addWidget(_noButton,5,1);

    _results = new QVector <ResultType> (_model->rowCount(),UNKNOW);
    qsrand(QDateTime::currentDateTime().toTime_t());

    startSession();
    nextCard();
    setLayout(layout);
    exec();
}

LearnDialog::~LearnDialog()
{
    endSession();
    delete _results;
    delete _mapper;
    delete _model;
}

void LearnDialog::startSession()
{
    QSqlQuery query (_db);
    query.prepare("INSERT INTO sessions (id, date_start) "
                   "VALUES (NULL, :start)");
    query.bindValue(":start",QDateTime::currentDateTime().toTime_t());
    query.exec();
}

void LearnDialog::endSession()
{
    QSqlQuery query (_db);
    query.prepare("UPDATE sessions SET date_end=:end WHERE id=(SELECT seq FROM SQLITE_SEQUENCE WHERE name=:session)");
    query.bindValue(":end",QDateTime::currentDateTime().toTime_t());
    query.bindValue(":session","sessions");
    query.exec();
}

void LearnDialog::nextCard()
{
    int index (0),
        currentIndex (_mapper->currentIndex()),
        cardsNumber  (_results->size()),
        cardsNotPass (cardsNumber - _results->count(PASS));

    if (cardsNotPass == 0) {
        accept();
        return;
    }

    do
    {
        index = 0 + (int)(qrand() / (double)RAND_MAX * (cardsNumber)); 
    }
    while (_results->at(index) == PASS || 
           (cardsNotPass > 1 && index == currentIndex));

    _index = index;
    _skipButton->setVisible(true);
    _showButton->setVisible(true);
    _yesButton->setVisible(false);
    _noButton->setVisible(false);
    _mapper->removeMapping(_viewData2);
    _mapper->removeMapping(_viewComment2);
    _viewData2->clear();
    _viewComment2->clear();
    _mapper->setCurrentIndex(_index);
}

void LearnDialog::skipCard()
{
    nextCard();
}

void LearnDialog::showAnswer()
{
    _skipButton->setVisible(false);
    _showButton->setVisible(false);
    _yesButton->setVisible(true);
    _noButton->setVisible(true);
    _mapper->addMapping(_viewData2,3);
    _mapper->addMapping(_viewComment2,4);
    _mapper->setCurrentIndex(_index);
}

void LearnDialog::addMark(bool isPass)
{
    QSqlQuery query (_db); 
    query.prepare("INSERT INTO marks (id, card_id, is_pass, date)"
                  "VALUES (NULL, :card_id, :is_pass, :date)");
    query.bindValue(":card_id",_model->record(_mapper->currentIndex()).value("id").toInt());
    query.bindValue(":is_pass",(isPass?1:0));
    query.bindValue(":date",QDateTime::currentDateTime().toTime_t());
    query.exec();
}

void LearnDialog::okCard()
{
    _results->replace(_mapper->currentIndex(),PASS);
    addMark(true);
    nextCard();
}

void LearnDialog::notOkCard()
{
    _results->replace(_mapper->currentIndex(),FAIL);
    addMark(false);
    nextCard();
}
