/**
 * @file  ConfigurationPage.h
 *
 * Widget for general configurations
 *
 */

#ifndef CONFIGURATIONPAGE_H
#define CONFIGURATIONPAGE_H

#include <QWidget>
#include <QLocale>

class QComboBox;

class ConfigurationPage : public QWidget
{
    Q_OBJECT

    public:
        ConfigurationPage(const QLocale &locale, QWidget *parent = 0);
        QLocale getLocale();

    private:
        QComboBox *_langCombo;
};

#endif /*CONFIGURATIONPAGE_H*/
