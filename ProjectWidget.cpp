#include <QtGui>
#include <QtSql>
#include "ProjectWidget.h"
#include "SqlException.h"
#include "DBConnection.h"
#include "MainWindow.h"

ProjectWidget::ProjectWidget (QWidget *parent)
    : QTableView(parent), _model(0)
{
}

ProjectWidget::~ProjectWidget()
{
    if (!_model) return;
	delete _model;
    DBConnection::closeDB(_fileName);
}

void ProjectWidget::retranslateUi()
{
    _model->setHeaderData(1, Qt::Horizontal, MainWindow::tr("Data 1"));
    _model->setHeaderData(2, Qt::Horizontal, MainWindow::tr("Comment 1"));
    _model->setHeaderData(3, Qt::Horizontal, MainWindow::tr("Data 2"));
    _model->setHeaderData(4, Qt::Horizontal, MainWindow::tr("Comment 2"));
    _model->setHeaderData(5, Qt::Horizontal, MainWindow::tr("Group"));
}

void ProjectWidget::load(const QString &fileName)
{
    _fileName = fileName;

    QSqlDatabase db = DBConnection::openDB(_fileName);
    createTables(db);

    _model = new QSqlTableModel (0,db);
    _model->setTable("cards");
	if (!_model->select())
		throw SqlException (_model->lastError());
    _model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    connect(_model, SIGNAL(dataChanged(const QModelIndex&,const QModelIndex&)),
            this, SIGNAL(modified()));
    connect(_model, SIGNAL(beforeInsert(QSqlRecord&)),
            this, SLOT(beforeAddCard(QSqlRecord&)));

    setModel(_model);
    setColumnHidden(0, true);
}

void ProjectWidget::save()
{
    if (!_model) return;
	if (!_model->submitAll())
		throw SqlException (_model->lastError());
}

void ProjectWidget::beforeAddCard(QSqlRecord &record)
{
    record.setValue(0,QVariant(QVariant::Int));
    record.setValue(5,1);
}

void ProjectWidget::addCard()
{
    _model->insertRow(_model->rowCount());
    QModelIndex index = _model->index(0,0);
    setCurrentIndex(index);
    edit(index);

    emit modified();
}

void ProjectWidget::removeCard()
{
    if (!currentIndex().isValid()) return;

    int row = currentIndex().row();
    _model->removeRow(row);
    hideRow(row);

    emit modified();
}

void ProjectWidget::createTables(const QSqlDatabase &db)
{
    QSqlQuery query (db);

    /*
    if (!query.exec("DROP TABLE IF EXISTS cards"))
    	throw SqlException (query.lastError());

    if (!query.exec("DROP TABLE IF EXISTS marks"))
    	throw SqlException (query.lastError());

    if (!query.exec("DROP TABLE IF EXISTS sessions"))
    	throw SqlException (query.lastError());
    */

    if (!query.exec("CREATE TABLE IF NOT EXISTS cards ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT,"
               "data1 TEXT NOT NULL,"
               "comment1 TEXT,"
               "data2 TEXT NOT NULL,"
               "comment2 TEXT,"
               "groupe INTEGER NOT NULL)"))
    	throw SqlException (query.lastError());

    /*
    if (!query.exec("CREATE TABLE IF NOT EXISTS marks ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT,"
               "card_id INTEGER NOT NULL,"
               "is_pass BOOL NOT NULL,"
               "date INTEGER NOT NULL)"))
    	throw SqlException (query.lastError());

    if (!query.exec("CREATE TABLE IF NOT EXISTS sessions ("
               "id INTEGER PRIMARY KEY AUTOINCREMENT,"
               "date_start INTEGER NOT NULL,"
               "date_end INTEGER)"))
    	throw SqlException (query.lastError());

    query.prepare("INSERT INTO cards (id, data1, data2) "
                   "VALUES (NULL, :data1, :data2)");
    query.bindValue(":data1", "house");
    query.bindValue(":data2", "maison");
    query.exec();
    query.bindValue(":data1", "tree");
    query.bindValue(":data2", "arbre");
    query.exec();
    */

    save();
}
