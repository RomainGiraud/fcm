/**
 * @file  LearnDialog.h
 *
 * Dialog box to learn the cards
 *
 */

#ifndef LEARNDIALOG_H
#define LEARNDIALOG_H

#include <QDialog>
#include <QVector>
#include <QSqlDatabase>

class QSqlQueryModel;
class QDataWidgetMapper;
class QTextEdit;
class QPushButton;

class LearnDialog : public QDialog
{
    Q_OBJECT

    public:
        LearnDialog(QString dbName, QWidget *parent=0);
        ~LearnDialog();

    private slots:
        void nextCard();
        void skipCard();
        void showAnswer();
        void okCard();
        void notOkCard();
        void addMark(bool isPass);

    private:
        int                _index;
        QSqlDatabase       _db;
        QSqlQueryModel    *_model;
        QDataWidgetMapper *_mapper;
        QTextEdit         *_viewData1,
                          *_viewComment1,
                          *_viewData2,
                          *_viewComment2;
        QPushButton       *_skipButton,
                          *_showButton,
                          *_yesButton,
                          *_noButton;
        enum ResultType { PASS=0, FAIL, UNKNOW };
        QVector <ResultType> *_results;
        void startSession();
        void endSession();
};

#endif /*LEARNDIALOG_H*/
