#include <QApplication>
#include <QPlastiqueStyle>
#include "MainWindow.h"

int main (int argc, char *argv[])
{
    QApplication::setStyle(new QPlastiqueStyle);
    QApplication app (argc,argv);

    MainWindow window;
    window.show();

    return app.exec();
}
